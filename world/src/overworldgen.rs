// Standard
use std::ops::{Add, Div, Mul, Neg, Sub};

// Library
use noise::{HybridMulti, MultiFractal, NoiseFn, Seedable, SuperSimplex};
use vek::*;

// Project
use common::terrain::chunk::Block;

// Local
use crate::{new_seed, Gen};

pub struct OverworldGen {
    land_nz: HybridMulti,
    dry_nz: HybridMulti,
    temp_nz: HybridMulti,

    hill_nz: HybridMulti,

    temp_vari_nz: SuperSimplex,
    alt_vari_nz: SuperSimplex,

    turb_x_nz: SuperSimplex,
    turb_y_nz: SuperSimplex,
}

#[derive(Copy, Clone)]
pub struct Out {
    pub land: f64,
    pub dry: f64,
    pub temp: f64,

    pub temp_vari: f64,
    pub alt_vari: f64,

    pub z_alt: f64,
    pub z_water: f64,
    pub z_sea: f64,
    pub z_hill: f64,
}

impl OverworldGen {
    pub fn new() -> Self {
        Self {
            // Large-scale
            land_nz: HybridMulti::new().set_seed(new_seed()).set_octaves(12),
            dry_nz: HybridMulti::new().set_seed(new_seed()).set_octaves(7),
            temp_nz: HybridMulti::new().set_seed(new_seed()).set_octaves(8),

            // Small-scale
            hill_nz: HybridMulti::new().set_seed(new_seed()).set_octaves(3),

            temp_vari_nz: SuperSimplex::new().set_seed(new_seed()),
            alt_vari_nz: SuperSimplex::new().set_seed(new_seed()),

            turb_x_nz: SuperSimplex::new().set_seed(new_seed()),
            turb_y_nz: SuperSimplex::new().set_seed(new_seed()),
        }
    }

    // -1 = deep ocean, 0 = sea level, 1 = mountain
    fn get_land(&self, pos: Vec2<f64>, dry: f64) -> f64 {
        let scale = 8000.0;

        let land = self.land_nz.get(pos.div(scale).into_array());
        //land - land.mul(5.0).sin().add(1.0).div(12.0)
        dry.max(0.01)
            * if land > 0.0 {
                land.powf(2.0) - land.powf(2.0).mul(35.0).sin().div(10.0).mul(land)
            } else {
                -land.powf(2.0)
            }
    }

    fn get_dry(&self, pos: Vec2<f64>) -> f64 {
        let scale = 1000.0;
        let vari_scale = scale / 1.0;
        let vari_ampl = scale / 6.0;

        let vari = self.dry_nz.get(pos.div(vari_scale).into_array()) * vari_ampl;

        let dry = 1.0 - pos.x.add(vari).div(scale).mul(3.14).sin().abs().powf(1.75);
        dry + dry.mul(15.0).cos().neg().add(1.0).div(15.0)
    }

    // 0 = cold, 0 = moderate, 1 = hot
    fn get_temp(&self, pos: Vec2<f64>) -> f64 {
        let scale = 10000.0;

        self.temp_nz.get(pos.div(scale).into_array()).add(1.0).div(2.0)
    }

    // 0 = lowest, 1 = highest
    fn get_hill(&self, pos: Vec2<f64>, dry: f64, land: f64) -> f64 {
        let scale = 800.0;

        let hill_basic = self.hill_nz.get(pos.div(scale).into_array());
        hill_basic //.mul(hill_basic.add(1.0).div(2.0)) * dry.min(land + 0.5).mul(4.0).min(1.0).max(0.1)
    }

    // 0 = no river, 1 = deep river
    fn get_river(&self, dry: f64) -> f64 {
        let frac = 0.002;
        if dry < frac {
            dry.div(frac).mul(3.14).cos().add(1.0).div(2.0)
        } else {
            0.0
        }
    }
}

impl Gen<()> for OverworldGen {
    type In = Vec2<i64>;
    type Out = Out;

    fn sample(&self, pos: Vec2<i64>, _: &()) -> Out {
        let pos_f64 = pos.map(|e| e as f64) * 1.0;

        let z_base = 126.0;
        let z_sea = 118.0;

        let dry = self.get_dry(pos_f64);
        let land = self.get_land(pos_f64, dry);
        let temp = self.get_temp(pos_f64);
        let river = self.get_river(dry);

        let hill = self.get_hill(pos_f64, dry, land);
        let z_hill = hill * 30.0 * dry.max(0.15);
        let z_land = z_base + land * 200.0 + z_hill;

        let z_height = z_land + z_hill; // + dry * 160.0 * (land * 1.0).min(1.0) + z_hill;
        let z_alt = z_height - river * 8.0;
        let z_water = (z_height - 3.0).max(z_sea);

        let temp_vari = self.temp_vari_nz.get(pos_f64.div(48.0).into_array());
        let alt_vari = self.alt_vari_nz.get(pos_f64.div(32.0).into_array());

        Out {
            land,
            dry,
            temp,

            temp_vari,
            alt_vari,

            z_alt,
            z_water,
            z_sea,
            z_hill,
        }
    }
}
