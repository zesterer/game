use gfx::{self, handle::Sampler, IndexBuffer, Slice};
use gfx_device_gl;

use crate::{
    consts::{ConstHandle, GlobalConsts},
    pipeline::Pipeline,
    renderer::{ColorFormat, ColorView, HdrShaderView, OurEncoder, Renderer},
};

pub type PipelineData = pipeline::Data<gfx_device_gl::Resources>;

gfx_defines! {
    pipeline pipeline {
        in_hdr: gfx::TextureSampler<[f32; 4]> = "t_Hdr",
        global_consts: gfx::ConstantBuffer<GlobalConsts> = "global_consts",
        out_color: gfx::RenderTarget<ColorFormat> = "target",
    }
}

pub fn render(
    encoder: &mut OurEncoder,
    hdr_shader: HdrShaderView,
    hdr_sampler: Sampler<gfx_device_gl::Resources>,
    out_color: ColorView,
    pipeline: &Pipeline<pipeline::Init<'static>>,
    global_consts: &ConstHandle<GlobalConsts>,
) {
    let data = PipelineData {
        in_hdr: (hdr_shader, hdr_sampler),
        global_consts: global_consts.buffer().clone(),
        out_color,
    };
    let slice = Slice::<gfx_device_gl::Resources> {
        start: 0,
        end: 3,
        base_vertex: 0,
        instances: None,
        buffer: IndexBuffer::Auto,
    };
    encoder.draw(&slice, pipeline.pso(), &data);
}
