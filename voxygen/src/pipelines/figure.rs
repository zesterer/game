// Library
use vek::*;

// Project
use common::terrain::figure::Cell;

// Local
use crate::{
    mesh::Mesh,
    volume::{MeshableVoxel, VoxelFace, VoxelLocality},
};

gfx_defines! {
    vertex Vertex {
        // Position relative to the bone origin
        pos: [f32; 3] = "vpos",
        // RGBA colour of the block
        col: [u8; 4] = "vcol",
        // The ID of the bone that this vertex is attached to
        bone: u8 = "vbone",
    }
}

impl MeshableVoxel for Cell {
    type Vertex = Vertex;

    fn mesh_face(&self, face: VoxelFace, mesh: &mut Mesh<Self::Vertex>) {
        unimplemented!();

        /*
        self.push_quad([
            Vertex { pos: quad.pos[0].map(|e| e as f32).into_array(), col: quad.col[0].into_array(), bone: quad.vox.bone },
            Vertex { pos: quad.pos[1].map(|e| e as f32).into_array(), col: quad.col[1].into_array(), bone: quad.vox.bone },
            Vertex { pos: quad.pos[2].map(|e| e as f32).into_array(), col: quad.col[2].into_array(), bone: quad.vox.bone },
            Vertex { pos: quad.pos[3].map(|e| e as f32).into_array(), col: quad.col[3].into_array(), bone: quad.vox.bone },
        ]);
        */
    }
}
