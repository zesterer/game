pub mod figure;
pub mod terrain;

pub trait Bucket {
    fn maintian(&mut self);
    fn render(&self);
}
