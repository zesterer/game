// Modules
pub mod element;
mod primitive;
mod render;
pub mod rescache;
pub mod span;
#[cfg(test)]
mod tests;

// Reexports
pub use self::span::Span;

// Standard
use std::rc::Rc;

// Library
use parking_lot::Mutex;
use vek::*;

// Local
use self::{element::Element, rescache::ResCache};
use crate::{renderer::Renderer, window::Event};

#[allow(dead_code)]
pub struct Ui {
    base: Rc<dyn Element>,
    rescache: Mutex<ResCache>,
}

impl Ui {
    #[allow(dead_code)]
    pub fn new(base: Rc<dyn Element>) -> Ui {
        Ui {
            base,
            rescache: Mutex::new(ResCache::new()),
        }
    }

    #[allow(dead_code)]
    pub fn render(&self, renderer: &mut Renderer) {
        self.base
            .render(renderer, &mut self.rescache.lock(), (Vec2::zero(), Vec2::one()));
    }

    #[allow(dead_code)]
    pub fn handle_event(&self, event: &Event, scr_res: Vec2<f32>) -> bool {
        let bounds = (Vec2::zero(), Vec2::one());

        match event {
            Event::MouseButton {
                state: _,
                button: _,
                pos,
            } => {
                self.base.activate_by_pos(pos / scr_res, scr_res, bounds);

                self.base.handle_local_event(event, scr_res, bounds)
            },
            Event::CursorAt(_) => self.base.handle_global_event(event, scr_res, bounds),
            _ => self.base.handle_local_event(event, scr_res, bounds),
        }
    }

    #[allow(dead_code)]
    pub fn activate_element(&self, element: &Rc<dyn Element>) -> bool { self.base.activate_element(element) }
}
