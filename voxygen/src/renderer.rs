// Library
use gfx::{
    self,
    format::Formatted,
    handle::{DepthStencilView, RenderTargetView, Sampler, ShaderResourceView},
    texture::{FilterMethod, SamplerInfo, WrapMode},
    Device, Encoder, Factory,
};
use gfx_device_gl;
use vek::*;

// Local
use crate::{
    consts::{ConstHandle, GlobalConsts},
    pipeline::Pipeline,
    shader::Shader,
    skybox, tonemapper, voxel,
};

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;
pub type HdrColorFormat = (gfx::format::R16_G16_B16_A16, gfx::format::Float);
pub type HdrDepthFormat = gfx::format::Depth32F;

pub type ColorView = RenderTargetView<gfx_device_gl::Resources, ColorFormat>;
pub type DepthView = DepthStencilView<gfx_device_gl::Resources, DepthFormat>;

pub type HdrShaderView = ShaderResourceView<gfx_device_gl::Resources, <HdrColorFormat as Formatted>::View>;
pub type HdrRenderView = RenderTargetView<gfx_device_gl::Resources, HdrColorFormat>;
pub type HdrDepthView = DepthStencilView<gfx_device_gl::Resources, HdrDepthFormat>;

pub type OurEncoder = Encoder<gfx_device_gl::Resources, gfx_device_gl::CommandBuffer>;

pub struct RendererInfo {
    pub vendor: String,
    pub model: String,
    pub gl_version: String,
}

pub struct Renderer {
    device: gfx_device_gl::Device,

    color_view: ColorView,
    depth_view: DepthView,

    encoder: OurEncoder,
    factory: gfx_device_gl::Factory,

    hdr_shader_view: HdrShaderView,
    hdr_render_view: HdrRenderView,
    hdr_depth_view: HdrDepthView,
    hdr_sampler: Sampler<gfx_device_gl::Resources>,

    skybox_pipeline: Pipeline<skybox::pipeline::Init<'static>>,
    volume_pipeline: voxel::VolumePipeline,
    tonemapper_pipeline: Pipeline<tonemapper::pipeline::Init<'static>>,
}

impl Renderer {
    pub fn new(
        device: gfx_device_gl::Device,
        mut factory: gfx_device_gl::Factory,
        color_view: ColorView,
        depth_view: DepthView,
        size: (u16, u16),
    ) -> Renderer {
        let (hdr_shader_view, hdr_render_view, hdr_depth_view, hdr_sampler) =
            Self::create_hdr_views(&mut factory, size);

        let volume_pipeline = voxel::VolumePipeline::new(&mut factory);

        let skybox_pipeline = Pipeline::new(
            &mut factory,
            skybox::pipeline::new(),
            &Shader::from_file("shaders/skybox/skybox.vert").expect("Could not load skybox vertex shader"),
            &Shader::from_file("shaders/skybox/skybox.frag").expect("Could not load skybox fragment shader"),
        );

        let tonemapper_pipeline = Pipeline::new(
            &mut factory,
            tonemapper::pipeline::new(),
            &Shader::from_file("shaders/tonemapper/tonemapper.vert").expect("Could not load skybox vertex shader"),
            &Shader::from_file("shaders/tonemapper/tonemapper.frag").expect("Could not load skybox fragment shader"),
        );

        Renderer {
            device,

            color_view,
            depth_view,

            hdr_shader_view,
            hdr_render_view,
            hdr_depth_view,
            hdr_sampler,

            encoder: factory.create_command_buffer().into(),
            factory,

            skybox_pipeline,
            volume_pipeline,
            tonemapper_pipeline,
        }
    }

    // TODO: Remove these methods
    pub fn volume_pipeline_mut(&mut self) -> &mut voxel::VolumePipeline { &mut self.volume_pipeline }
    pub fn flush_pipelines(&mut self) {
        self.volume_pipeline
            .flush(&self.hdr_render_view, &self.hdr_depth_view, &mut self.encoder);
    }

    pub fn get_info(&self) -> RendererInfo {
        let info = self.device.get_info();
        RendererInfo {
            vendor: String::from(info.platform_name.vendor),
            model: String::from(info.platform_name.renderer),
            gl_version: format!("{}.{}", info.version.major, info.version.minor),
        }
    }

    pub fn create_hdr_views(
        factory: &mut gfx_device_gl::Factory,
        size: (u16, u16),
    ) -> (
        HdrShaderView,
        HdrRenderView,
        HdrDepthView,
        Sampler<gfx_device_gl::Resources>,
    ) {
        let (_, hdr_shader_view, hdr_render_view) =
            factory.create_render_target::<HdrColorFormat>(size.0, size.1).unwrap();
        let hdr_sampler = factory.create_sampler(SamplerInfo::new(FilterMethod::Scale, WrapMode::Clamp));
        let hdr_depth_view = factory
            .create_depth_stencil_view_only::<HdrDepthFormat>(size.0, size.1)
            .unwrap();
        (hdr_shader_view, hdr_render_view, hdr_depth_view, hdr_sampler)
    }

    pub fn begin_frame(&mut self, clear_color: Option<Vec3<f32>>) {
        self.encoder.clear_depth(&self.hdr_depth_view, 1.0);
    }

    /*
    pub fn queue_terrain_model(&mut self, ) {
    
    }
    */

    pub fn queue_skybox_model(&mut self, model: &skybox::Model, global_consts: &ConstHandle<GlobalConsts>) {
        model.render(
            &mut self.encoder,
            self.hdr_render_view.clone(),
            self.hdr_depth_view.clone(),
            &self.skybox_pipeline,
            &global_consts,
        );
    }

    pub fn queue_tonemapper(&mut self, global_consts: &ConstHandle<GlobalConsts>) {
        tonemapper::render(
            &mut self.encoder,
            self.hdr_shader_view.clone(),
            self.hdr_sampler.clone(),
            self.color_view.clone(),
            &self.tonemapper_pipeline,
            &global_consts,
        );
    }

    pub fn end_frame(&mut self) {
        self.encoder.flush(&mut self.device);
        self.device.cleanup();
    }

    // TODO: Remove these functions
    #[allow(dead_code)]
    pub fn encoder(&self) -> &OurEncoder { &self.encoder }
    #[allow(dead_code)]
    pub fn encoder_mut(&mut self) -> &mut OurEncoder { &mut self.encoder }

    #[allow(dead_code)]
    pub fn factory(&self) -> &gfx_device_gl::Factory { &self.factory }
    #[allow(dead_code)]
    pub fn factory_mut(&mut self) -> &mut gfx_device_gl::Factory { &mut self.factory }

    #[allow(dead_code)]
    pub fn color_view(&self) -> &ColorView { &self.color_view }
    #[allow(dead_code)]
    pub fn depth_view(&self) -> &DepthView { &self.depth_view }

    pub fn get_view_resolution(&self) -> Vec2<u16> {
        Vec2::new(
            self.hdr_render_view.get_dimensions().0,
            self.hdr_render_view.get_dimensions().1,
        )
    }

    #[allow(dead_code)]
    pub fn set_views(&mut self, color_view: ColorView, depth_view: DepthView, size: (u16, u16)) {
        let (hdr_shader_view, hdr_render_view, hdr_depth_view, hdr_sampler) =
            Self::create_hdr_views(&mut self.factory, size);
        self.hdr_shader_view = hdr_shader_view;
        self.hdr_render_view = hdr_render_view;
        self.hdr_depth_view = hdr_depth_view;
        self.hdr_sampler = hdr_sampler;
        self.color_view = color_view;
        self.depth_view = depth_view;
    }
}
