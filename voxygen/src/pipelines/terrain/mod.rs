pub mod mesh;
pub mod model;

// Standard
use std::collections::HashMap;

// Libraries
use vek::*;

// Local
use self::model::TerrainModel;
use crate::pipelines::Bucket;

pub struct TerrainBucket {
    layers: HashMap<Vec3<i32>, (TerrainModel, TerrainModel)>,
}

impl TerrainBucket {
    pub fn add_chunk_models(&mut self, pos: Vec3<i32>, solid: TerrainModel, trans: TerrainModel) {
        self.layers.insert(pos, (solid, trans));
    }
}

impl Bucket for TerrainBucket {
    fn maintian(&mut self) {
        // TODO
    }

    fn render(&self) {
        // TODO
    }
}
