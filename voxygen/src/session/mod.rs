mod terrain;

// Standard
use std::{f32::consts::PI, net::ToSocketAddrs};

// Library
use dot_vox;
use vek::*;

// Project
use client::{self, Client, ClientEvent, PlayMode, CHUNK_SIZE};
use common::{
    get_asset_path,
    terrain::{voloffs_to_voxabs, Container},
    util::manager::Manager,
};

// Local
use crate::{
    audio::frontend::AudioFrontend,
    camera::Camera,
    consts::{ConstHandle, GlobalConsts},
    get_build_time, get_git_hash,
    hud::{Hud, HudEvent},
    key_state::KeyState,
    renderer::Renderer,
    skybox, voxel,
    window::Event,
};

use self::terrain::TerrainMgr;

// TODO: Get rid of this
mod payload {
    use super::*;
    use common::terrain::{
        chunk::{Chunk, ChunkContainer},
        Container, VolOffs,
    };
    use fnv::FnvBuildHasher;
    use indexmap::IndexMap;
    use parking_lot::Mutex;
    use std::sync::Arc;

    type FnvIndexMap<K, V> = IndexMap<K, V, FnvBuildHasher>;

    pub enum ChunkPayload {
        Meshes(FnvIndexMap<voxel::MaterialKind, voxel::Mesh>),
        Model {
            model: voxel::Model,
            model_consts: ConstHandle<voxel::ModelConsts>,
        },
    }

    pub struct EntityPayload {
        pub pos: Vec3<f32>,
        pub consts: ConstHandle<voxel::ModelConsts>,
    }

    pub struct Payloads {}
    impl client::Payloads for Payloads {
        type Chunk = ChunkPayload;
        type Entity = EntityPayload;
        type Audio = AudioFrontend;
    }

    pub fn gen_payload(
        _key: Vec3<VolOffs>,
        con: Arc<Mutex<Option<ChunkContainer<<Payloads as client::Payloads>::Chunk>>>>,
    ) {
        let conlock = con.lock();
        if let Some(ref con) = *conlock {
            *con.payload_mut() = Some(ChunkPayload::Meshes(match *con.data() {
                Chunk::Homo(ref homo) => voxel::Mesh::from(homo),
                Chunk::Hetero(ref hetero) => voxel::Mesh::from(hetero),
                Chunk::Rle(ref rle) => voxel::Mesh::from(rle),
                Chunk::HeteroAndRle(ref hetero, _) => voxel::Mesh::from(hetero),
            }));
        }
    }
}

// Utility function
// TODO: Find a way to get rid of this ugliness
fn to_4x4(m: &Mat4<f32>) -> [[f32; 4]; 4] {
    let mut out = [[0.0; 4]; 4];
    (0..4).for_each(|i| (0..4).for_each(|j| out[i][j] = m[(j, i)]));
    out
}

#[derive(Copy, Clone, Default)]
pub struct InputState {
    pub key_right: bool,
    pub key_left: bool,
    pub key_up: bool,
    pub key_down: bool,
    pub key_jump: bool,

    pub toggle_hud: bool,

    pub cam_ori_delta: Vec2<f32>,
    pub cam_zoom_delta: f32,
    pub cam_aspect_ratio: f32,
}

impl InputState {
    pub fn new() -> Self {
        Self {
            cam_aspect_ratio: 1.618,
            ..Self::default()
        }
    }

    pub fn reset(&mut self) {
        self.toggle_hud = false;

        self.cam_ori_delta = Vec2::zero();
        self.cam_zoom_delta = 0.0;
    }

    pub fn dir_vec(&self) -> Vec2<f32> {
        Vec2::new(
            if self.key_right { 1.0 } else { 0.0 } + if self.key_left { -1.0 } else { 0.0 },
            if self.key_up { 1.0 } else { 0.0 } + if self.key_down { -1.0 } else { 0.0 },
        )
    }
}

pub struct Session {
    client: Manager<Client<payload::Payloads>>,

    terrain_mgr: TerrainMgr,

    global_consts: ConstHandle<GlobalConsts>,
    camera: Camera,

    hud: Hud,
    audio: Manager<AudioFrontend>,

    skybox_model: skybox::Model,
    player_model: voxel::Model,
}

impl Session {
    pub fn new<A: ToSocketAddrs>(
        mode: PlayMode,
        alias: String,
        remote: A,
        vd: i64,
        renderer: &mut Renderer,
    ) -> Result<Self, client::Error> {
        let skybox_model = skybox::Model::new(renderer, &skybox::Mesh::new_skybox());

        // Create player model
        // TODO: Replace this with something better
        let player_model = {
            let player_vox = dot_vox::load(
                get_asset_path("voxygen/cosmetic/creature/friendly/knight.vox")
                    .to_str()
                    .unwrap(),
            )
            .expect("cannot find model player6.vox. Make sure to start voxygen from its folder");
            let player_mesh =
                voxel::Mesh::from_with_offset(&voxel::vox_to_figure(player_vox), Vec3::new(-10.0, -4.0, 0.0), false);
            voxel::Model::new(renderer, &player_mesh)
        };

        // Create HUD
        let hud = {
            let hud = Hud::new();
            hud.debug_box()
                .version_label
                .set_text(format!("Version: {}", env!("CARGO_PKG_VERSION")));
            hud.debug_box()
                .githash_label
                .set_text(format!("Git hash: {}", &get_git_hash().get(..8).unwrap_or("<none>")));
            hud.debug_box()
                .buildtime_label
                .set_text(format!("Build time: {}", get_build_time()));
            hud
        };

        let audio = AudioFrontend::new();

        // Create client
        let client = Client::new(
            mode,
            alias,
            remote,
            payload::gen_payload,
            |_, _| {},
            Manager::<AudioFrontend>::internal(&audio).clone(),
            vd,
        )?;

        Ok(Self {
            client,

            terrain_mgr: TerrainMgr::new(),

            global_consts: ConstHandle::new(renderer),
            camera: Camera::new(),

            hud,
            audio,

            skybox_model,
            player_model,
        })
    }

    pub fn handle_event(&mut self, event: &Event, scr_res: Vec2<f32>) -> bool { self.hud.handle_event(event, scr_res) }

    pub fn handle_input(&mut self, input: &InputState) {
        // Handle camera input
        self.camera.rotate_by(input.cam_ori_delta);
        self.camera.zoom_by(input.cam_zoom_delta);
        self.camera.set_aspect_ratio(input.cam_aspect_ratio);

        // Toggle the HUD if needed
        if input.toggle_hud {
            self.hud.toggle_visible();
        }

        // TODO: Refactor this method
        // Calculate movement player movement vector
        let ori = *self.camera.ori();
        let unit_vecs = (
            Vec2::new(ori.x.cos(), -ori.x.sin()),
            Vec2::new(ori.x.sin(), ori.x.cos()),
        );
        let dir_vec = input.dir_vec();
        let mov_vec = unit_vecs.0 * dir_vec.x + unit_vecs.1 * dir_vec.y;

        // Why do we do this in Voxygen?!
        const LOOKING_VEL_FAC: f32 = 1.0;
        const LOOKING_CTRL_ACC_FAC: f32 = 1.0;
        const MIN_LOOKING: f32 = 0.5;
        const LEANING_FAC: f32 = 0.05;
        if let Some(player_entity) = self.client.player_entity() {
            let mut player_entity = player_entity.write();

            // Apply acceleration
            player_entity.ctrl_acc_mut().x = mov_vec.x;
            player_entity.ctrl_acc_mut().y = mov_vec.y;

            // Apply jumping
            player_entity.ctrl_acc_mut().z = if input.key_jump { 1.0 } else { 0.0 };

            let looking = (*player_entity.vel() * LOOKING_VEL_FAC
                + *player_entity.ctrl_acc_mut() * LOOKING_CTRL_ACC_FAC)
                / (LOOKING_VEL_FAC + LOOKING_CTRL_ACC_FAC);

            // Apply rotating
            if looking.magnitude() > MIN_LOOKING {
                player_entity.look_dir_mut().x = looking.x.atan2(looking.y);
            }

            // Apply leaning
            player_entity.look_dir_mut().y = Vec2::new(looking.x, looking.y).magnitude() * LEANING_FAC;
        }
    }

    pub fn maintain(&mut self, renderer: &mut Renderer, last_fps: usize) {
        // Handle all client events
        self.client.get_events().into_iter().for_each(|event| match event {
            ClientEvent::RecvChatMsg { text } => self.hud.chat_box().add_chat_msg(text),
        });

        // Update camera to focus on the player's current position
        let player_pos = self
            .client
            .player_entity()
            .map(|p| *p.read().pos() + Vec3::new(0.0, 0.0, 1.75))
            .unwrap_or(Vec3::new(0.0, 0.0, 0.0));
        // Gently move to the new focus to avoid camera "jittering"
        self.camera
            .set_focus(self.camera.get_focus() * 0.75 + player_pos * 0.25);

        // Update global constants that apply to the entire frame
        let camera_mats = self.camera.get_mats();
        let cam_pos = self.camera.get_pos(Some(&camera_mats));
        let focus = self.camera.get_focus();
        self.global_consts.update(
            renderer,
            GlobalConsts {
                view_mat: to_4x4(&camera_mats.0),
                proj_mat: to_4x4(&camera_mats.1),
                cam_origin: [cam_pos.x, cam_pos.y, cam_pos.z, 1.0],
                play_origin: [focus.x, focus.y, focus.z, 1.0], // TODO: Change to `focus`
                view_distance: [self.client.view_distance(); 4],
                time: [self.client.time().as_float_secs() as f32; 4],
            },
        );

        // Maintain entity consts
        {
            // Take the physics lock to sync client and frontend updates
            let _ = self.client.take_phys_lock();

            // Update each entity constbuffer
            for (uid, entity) in self.client.entities().iter() {
                let mut entity = entity.write();

                // Get this now, because the borrow checker won't let us after the next line
                let entity_pos = *entity.pos();
                let entity_vel = *entity.vel();
                let entity_look_dir = *entity.look_dir();

                if Some(*uid) == self.client.player().entity_uid() {
                    //update audio
                    self.audio
                        .set_pos(entity_pos, entity_vel, camera_mats.0 * camera_mats.1);
                }

                // Find the entity's payload (or give it one if it doesn't have one)
                let mut payload = entity.payload_mut().get_or_insert_with(|| payload::EntityPayload {
                    pos: Vec3::zero(),
                    consts: ConstHandle::new(renderer),
                });

                // Smoothly update the supposed position
                payload.pos = payload.pos * 0.7 + entity_pos * 0.3;

                // Calculate entity model matrix using the supposed position
                let model_mat = Mat4::<f32>::translation_3d(Vec3::from(payload.pos.into_array()))
                    * Mat4::rotation_z(PI - entity_look_dir.x)
                    * Mat4::rotation_x(entity_look_dir.y);

                // Update the model const buffer (its payload)
                // TODO: Put the model into the payload so we can have per-entity models!
                payload.consts.update(
                    renderer,
                    voxel::ModelConsts {
                        model_mat: to_4x4(&model_mat),
                    },
                );
            }
        }

        // Maintain chunk consts
        for (&pos, con) in self.client.chunk_mgr().pers(|_| true).iter() {
            let trylock = &mut con.payload_try_mut(); //we try to lock it, if it is already written to we just ignore this chunk for a frame
            if let Some(ref mut lock) = trylock {
                //sometimes payload does not exist, dont render then
                if let Some(ref mut payload) = **lock {
                    if let payload::ChunkPayload::Meshes(ref mut mesh) = payload {
                        // Create and update new model constants for the chunk
                        let model_consts = ConstHandle::new(renderer);
                        model_consts.update(
                            renderer,
                            voxel::ModelConsts {
                                model_mat: to_4x4(&Mat4::translation_3d(
                                    voloffs_to_voxabs(pos, CHUNK_SIZE).map(|e| e as f32),
                                )),
                            },
                        );

                        // Update the chunk's payload
                        *payload = payload::ChunkPayload::Model {
                            model: voxel::Model::new(renderer, mesh),
                            model_consts,
                        };
                    }
                }
            }
        }

        // Maintain the terrain rendering structure
        self.terrain_mgr.maintain(renderer, &self.client);

        // Tick the client
        self.client.schedule_tick();

        // Update HUD elements
        self.hud.debug_box().fps_label.set_text(format!("FPS: {}", last_fps));
        let pos_text = self
            .client
            .player_entity()
            .map(|p| format!("Pos: {}", p.read().pos().map(|e| e as i64)))
            .unwrap_or("Unknown position".to_string());
        self.hud.debug_box().pos_label.set_text(pos_text);

        // Handle all HUD events
        self.hud.get_events().into_iter().for_each(|event| match event {
            HudEvent::ChatMsgSent { text } if text.len() > 0 => self.client.send_chat_msg(text),
            _ => {},
        });
    }

    pub fn render(&self, renderer: &mut Renderer) {
        // We render things in order of their distance to the camera. This allows us to skip
        // a lot of fragments that fall behind already drawn ones.

        // Render each entity
        for (&uid, entity) in self.client.entities().iter() {
            if self.client.player().entity_uid.map(|p| p == uid).unwrap_or(false) && self.camera.get_zoom() == 0.0 {
                continue;
            }

            if let Some(payload) = entity.read().payload() {
                renderer
                    .volume_pipeline_mut()
                    .draw_model(&self.player_model, &payload.consts, &self.global_consts);
            }
        }

        // Render each chunk
        let camera_mats = self.camera.get_mats();
        let cam_pos = self.camera.get_pos(Some(&camera_mats));
        let squared_vd = self.client.view_distance().powi(2) as f32;
        let cam_vec_world = camera_mats.0.inverted() * (-Vec4::unit_z());
        let focus = self.camera.get_focus();
        for (_pos, con) in self
            .client
            .chunk_mgr()
            .pers(|chunk_offs| {
                let chunk_pos = chunk_offs.map(|e| e as f32) * CHUNK_SIZE.map(|e| e as f32);
                // This limit represents the point in the chunk that's closest to the player (0 - CHUNK_SIZE)
                let chunk_offs_limit = Vec3::clamp(focus - chunk_pos, Vec3::zero(), CHUNK_SIZE.map(|e| e as f32));
                // Check whether the chunk is within range of the view distance
                (chunk_pos + chunk_offs_limit).distance_squared(focus) < squared_vd &&
                // Check whether the chunk is within the frustrum of the camera (or within a certain minimum range to avoid visual artefacts)
                (Vec4::from(chunk_pos + CHUNK_SIZE.map(|e| e as f32) / 2.0 - cam_pos)
                        .normalized()
                        .dot(cam_vec_world)
                        > self.camera.get_fov().cos()
                        || (chunk_pos + CHUNK_SIZE.map(|e| e as f32) / 2.0 - cam_pos).magnitude()
                            < CHUNK_SIZE.x as f32 * 2.0)
            })
            .iter()
        {
            if let Some(ref lock) = con.payload_try() {
                // If the payload is locked, ignore it to avoid throttling things
                if let Some(ref payload) = **lock {
                    if let payload::ChunkPayload::Model {
                        ref model,
                        ref model_consts,
                    } = payload
                    {
                        renderer
                            .volume_pipeline_mut()
                            .draw_model(&model, model_consts, &self.global_consts);
                    }
                }
            }
        }

        // Render terrain
        self.terrain_mgr.render(renderer);

        // Render the skybox
        renderer.queue_skybox_model(&self.skybox_model, &self.global_consts);

        // Flush voxel pipeline draws
        // TODO: Don't use this
        renderer.flush_pipelines();

        // Tonemapping is the post-processing stage
        renderer.queue_tonemapper(&self.global_consts);

        // The HUD is displayed after post-processing
        self.hud.render(renderer);
    }
}
