// Standard
use std::{
    cell::RefCell,
    f32::consts::PI,
    mem,
    rc::Rc,
    sync::atomic::{AtomicBool, Ordering},
};

// Library
use vek::*;

// Project
use common::get_asset_path;

// Local
use crate::{
    camera::Camera,
    consts::{ConstHandle, GlobalConsts},
    renderer::Renderer,
    session::InputState,
    skybox,
    ui::{
        element::{Button, Element, HBox, Label, TextBox, VBox, WinBox},
        Span, Ui,
    },
    voxel,
    window::Event,
};

pub enum MainMenuEvent {
    Play { remote: String, alias: String, vd: i64 },
}

// Utility function
// TODO: Find a way to get rid of this ugliness
fn to_4x4(m: &Mat4<f32>) -> [[f32; 4]; 4] {
    let mut out = [[0.0; 4]; 4];
    (0..4).for_each(|i| (0..4).for_each(|j| out[i][j] = m[(j, i)]));
    out
}

pub struct MainMenu {
    global_consts: ConstHandle<GlobalConsts>,
    camera: Camera,
    cursor_jitter: Vec2<f32>,
    skybox_model: skybox::Model,

    logo_model: voxel::Model,
    logo_consts: ConstHandle<voxel::ModelConsts>,

    garden_model: voxel::Model,
    garden_consts: ConstHandle<voxel::ModelConsts>,

    events: Rc<RefCell<Vec<MainMenuEvent>>>,
    ui: Ui,
}

impl MainMenu {
    pub fn new(renderer: &mut Renderer) -> Self {
        let winbox = WinBox::new().with_color(Rgba::new(0.0, 0.0, 0.0, 0.0));

        let events = Rc::new(RefCell::new(vec![]));

        let remote_textbox = TextBox::new()
            .with_color(Rgba::new(1.0, 1.0, 1.0, 1.0))
            .with_background_color(Rgba::new(1.0, 1.0, 1.0, 0.01))
            .with_text("veloren.pftclan.de:38888".to_string())
            .with_margin(Span::px(16, 16));

        let alias_textbox = TextBox::new()
            .with_color(Rgba::new(1.0, 1.0, 1.0, 1.0))
            .with_background_color(Rgba::new(1.0, 1.0, 1.0, 0.01))
            .with_text("Test".to_string())
            .with_margin(Span::px(16, 16));

        let events_ref = events.clone();
        let remote_ref = remote_textbox.clone();
        let alias_ref = alias_textbox.clone();
        let play_button = Button::new()
            .with_color(Rgba::new(0.0, 0.0, 0.0, 0.8))
            .with_hover_color(Rgba::new(0.0, 0.1, 0.0, 0.8))
            .with_click_color(Rgba::new(0.0, 0.2, 0.0, 0.8))
            .with_click_fn(move |_| {
                events_ref.borrow_mut().push(MainMenuEvent::Play {
                    remote: remote_ref.get_text().clone(),
                    alias: alias_ref.get_text().clone(),
                    vd: 400,
                });
            })
            .with_child(
                Label::new()
                    .with_text("Connect to server".to_string())
                    .with_color(Rgba::new(1.0, 1.0, 1.0, 1.0))
                    .with_margin(Span::px(16, 16)),
            );

        winbox.add_child_at(
            Span::bottom() + Span::px(0, -16),
            Span::bottom(),
            Span::px(200, 48),
            play_button,
        );

        winbox.add_child_at(
            Span::center() + Span::px(0, -16),
            Span::bottom(),
            Span::px(400, 48),
            HBox::new()
                .with_color(Rgba::new(0.0, 0.0, 0.0, 0.8))
                .with_child(
                    Label::new()
                        .with_text("Server Address".to_string())
                        .with_color(Rgba::new(1.0, 1.0, 1.0, 1.0))
                        .with_margin(Span::px(16, 16)),
                )
                .with_child(remote_textbox),
        );

        winbox.add_child_at(
            Span::center() + Span::px(0, 16),
            Span::top(),
            Span::px(400, 48),
            HBox::new()
                .with_color(Rgba::new(0.0, 0.0, 0.0, 0.8))
                .with_child(
                    Label::new()
                        .with_text("Alias".to_string())
                        .with_color(Rgba::new(1.0, 1.0, 1.0, 1.0))
                        .with_margin(Span::px(16, 16)),
                )
                .with_child(alias_textbox),
        );

        let skybox_model = skybox::Model::new(renderer, &skybox::Mesh::new_skybox());

        let logo_model = {
            let logo_vox = dot_vox::load(get_asset_path("voxygen/menu/logo.vox").to_str().unwrap())
                .expect("// TODO: Get rid of this except");
            let logo_mesh =
                voxel::Mesh::from_with_offset(&voxel::vox_to_figure(logo_vox), Vec3::new(-61.5, -18.5, 0.0), false);
            voxel::Model::new(renderer, &logo_mesh)
        };

        let garden_model = {
            let garden_vox = dot_vox::load(get_asset_path("voxygen/menu/garden.vox").to_str().unwrap())
                .expect("// TODO: Get rid of this except");
            let garden_mesh = voxel::Mesh::from_with_offset(&voxel::vox_to_figure(garden_vox), Vec3::zero(), false);
            voxel::Model::new(renderer, &garden_mesh)
        };

        Self {
            global_consts: ConstHandle::new(renderer),
            camera: Camera::new(),
            cursor_jitter: Vec2::zero(),
            skybox_model,

            logo_model,
            logo_consts: ConstHandle::new(renderer),

            garden_model,
            garden_consts: ConstHandle::new(renderer),

            events,

            ui: Ui::new(winbox),
        }
    }

    pub fn get_events(&self) -> Vec<MainMenuEvent> {
        let mut events = vec![];
        mem::swap(&mut *self.events.borrow_mut(), &mut events);
        events
    }

    pub fn handle_input(&mut self, input: &InputState) { self.camera.set_aspect_ratio(input.cam_aspect_ratio); }

    pub fn handle_event(&mut self, event: &Event, scr_res: Vec2<f32>) -> bool {
        match event {
            Event::CursorAt(pos) => self.cursor_jitter = (pos / scr_res) * 0.05 - 0.025,
            _ => {},
        }

        self.ui.handle_event(event, scr_res);
        true // The menu *always* handles events
    }

    pub fn maintain(&mut self) {
        self.camera.set_focus(Vec3::new(101.0, 28.0, 8.0));
        self.camera
            .set_ori(Vec2::new(-172.0 * PI - 0.5, 0.0) + self.cursor_jitter);
        self.camera.set_zoom(14.0);
    }

    pub fn render(&self, renderer: &mut Renderer) {
        // Update global constants that apply to the entire frame
        let camera_mats = self.camera.get_mats();
        let cam_pos = self.camera.get_pos(Some(&camera_mats));
        let focus = self.camera.get_focus();
        self.global_consts.update(
            renderer,
            GlobalConsts {
                view_mat: to_4x4(&camera_mats.0),
                proj_mat: to_4x4(&camera_mats.1),
                cam_origin: [cam_pos.x, cam_pos.y, cam_pos.z, 1.0],
                play_origin: [focus.x, focus.y, focus.z, 1.0], // TODO: Change to `focus`
                view_distance: [150.0; 4],
                time: [120.0; 4],
            },
        );

        // Render the skybox
        renderer.queue_skybox_model(&self.skybox_model, &self.global_consts);

        // Render the garden
        self.garden_consts.update(
            renderer,
            voxel::ModelConsts {
                model_mat: to_4x4(&Mat4::<f32>::scaling_3d(Vec3::broadcast(1.0 / 0.085))),
            },
        );
        renderer
            .volume_pipeline_mut()
            .draw_model(&self.garden_model, &self.garden_consts, &self.global_consts);

        // Render the logo
        self.logo_consts.update(
            renderer,
            voxel::ModelConsts {
                model_mat: to_4x4(
                    &(Mat4::<f32>::translation_3d(self.camera.get_focus() + Vec3::new(0.0, 0.0, 2.5))
                        * Mat4::rotation_z(200.0 * PI - self.camera.ori().x)
                        * Mat4::<f32>::scaling_3d(Vec3::broadcast(1.8))),
                ),
            },
        );
        renderer
            .volume_pipeline_mut()
            .draw_model(&self.logo_model, &self.logo_consts, &self.global_consts);

        // Flush voxel pipeline draws
        // TODO: Don't use this
        renderer.flush_pipelines();

        // Tonemapping is the post-processing stage
        renderer.queue_tonemapper(&self.global_consts);

        // Render the UI on top of everything else
        self.ui.render(renderer);
    }
}
