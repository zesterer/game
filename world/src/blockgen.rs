// Standard
use std::ops::{Add, Div, Mul, Sub};

// Library
use noise::{HybridMulti, MultiFractal, NoiseFn, Seedable, SuperSimplex};
use vek::*;

// Project
use common::terrain::chunk::Block;

// Local
use crate::{
    cachegen::CacheGen,
    new_seed,
    overworldgen::{Out as OverworldOut, OverworldGen},
    towngen::{self, TownGen},
    Gen,
};

pub struct BlockGen {
    overworld_gen: CacheGen<OverworldGen, Vec2<i64>, OverworldOut>,
    town_gen: TownGen,
    warp_nz: HybridMulti,
    cliff_nz: HybridMulti,
    cliff_vari_nz: SuperSimplex,

    turb_x_nz: SuperSimplex,
    turb_y_nz: SuperSimplex,
}

impl BlockGen {
    pub fn new() -> Self {
        Self {
            overworld_gen: CacheGen::new(OverworldGen::new(), 4096),
            town_gen: TownGen::new(),

            warp_nz: HybridMulti::new().set_seed(new_seed()).set_octaves(3),
            cliff_nz: HybridMulti::new().set_seed(new_seed()).set_octaves(3),
            cliff_vari_nz: SuperSimplex::new().set_seed(new_seed()),

            turb_x_nz: SuperSimplex::new().set_seed(new_seed()),
            turb_y_nz: SuperSimplex::new().set_seed(new_seed()),
        }
    }

    pub fn get_invariant_z(&self, pos: Vec2<i64>) -> (OverworldOut, towngen::InvariantZ) {
        let overworld = self.overworld_gen.sample(pos, &());

        (
            overworld,
            self.town_gen
                .get_invariant_z(pos, (&overworld, &self.overworld_gen.internal())),
        )
    }

    fn get_warp(&self, pos: Vec3<f64>, dry: f64, land: f64) -> f64 {
        let scale = Vec3::new(350.0, 350.0, 300.0);
        let factor = 1.2;

        self.warp_nz
            .get(pos.div(scale).into_array())
            .abs()
            .sub(0.2)
            .max(0.0)
            .mul((dry - 0.2).min(land).min(0.4) * factor)
            .max(0.0)
    }

    fn get_cliff(&self, pos: Vec3<f64>, dry: f64, land: f64, overworld: &OverworldOut) -> f64 {
        let scale = Vec3::new(650.0, 650.0, 5000.0);
        let factor = 50.0;
        let max_height = 0.2;

        let dry_close = Vec2::new(dry * 4000.0, pos.z - overworld.z_water).magnitude();

        self.cliff_nz
            .get(pos.div(scale).into_array())
            .sub(0.55)
            .add(self.cliff_vari_nz.get(pos.div(40.0).into_array()) * 0.11)
            .mul(factor)
            .mul(0.5 + (pos.z - overworld.z_water).max(0.0) * 0.05)
            .mul(if dry_close < 12.0 { 0.0 } else { 1.0 })
            .max(0.0)
            .min(1.0)
    }
}

impl Gen<(OverworldOut, towngen::InvariantZ)> for BlockGen {
    type In = Vec3<i64>;
    type Out = Block;

    fn sample<'a>(
        &self,
        pos: Vec3<i64>,
        (overworld, towngen_invariant_z): &(OverworldOut, towngen::InvariantZ),
    ) -> Block {
        let overworld = self.overworld_gen.sample(Vec2::from(pos), &());

        let pos_f64 = pos.map(|e| e as f64) * 1.0;

        let Z_WARP_MAX = 96.0;
        let warp = if pos_f64.z > overworld.z_alt || pos_f64.z < overworld.z_alt + Z_WARP_MAX {
            self.get_warp(pos_f64, overworld.dry, overworld.land)
        } else {
            0.0
        };
        let z_warp = warp.mul(Z_WARP_MAX);
        let cliff = self.get_cliff(pos_f64, overworld.dry, overworld.land, &overworld);

        let town = self
            .town_gen
            .sample(pos, &(towngen_invariant_z, &overworld, self.overworld_gen.internal()));

        let z_alt = overworld.z_alt + z_warp - town.surface.map(|_| 1.0).unwrap_or(0.0);

        let cliff_top = 100.0 + overworld.z_water.div(80.0).add(warp).round().mul(80.0) * 0.4 + overworld.z_hill * 2.0;
        let z_alt = (z_alt + cliff * 256.0).min(cliff_top).max(z_alt);

        const GRASS_DEPTH: f64 = 2.5;

        if pos_f64.z < z_alt {
            if pos_f64.z < overworld.z_sea + 2.0 {
                Block::SAND
            } else if pos_f64.z < overworld.z_water - 1.0 {
                Block::EARTH
            } else if (pos_f64.z > z_alt - GRASS_DEPTH && cliff == 0.0) || pos_f64.z > cliff_top - 1.5 {
                town.surface.unwrap_or(if overworld.temp > 0.5 {
                    Block::gradient3(
                        Block::GRAD3_O_STONE,
                        Block::GRAD3_A_GRASS,
                        Block::GRAD3_B_SAND,
                        {
                            let vari = 0.12;
                            (overworld.temp.sub(0.65).mul(16.0) - (1.0 - overworld.dry).powf(8.0).mul(4.0))
                                .max(vari)
                                .min(1.0 - vari)
                                .add(overworld.temp_vari * vari)
                                .max(0.0)
                                .min(1.0)
                                .mul(32.0) as u8
                        },
                        {
                            let vari = 0.1;
                            ((650.0 - (z_alt - overworld.z_sea)).div(400.0) - z_warp * 0.1)
                                .max(vari)
                                .min(1.0 - vari)
                                .add(overworld.alt_vari * vari)
                                .max(0.0)
                                .min(1.0)
                                .mul(64.0) as u8
                        },
                    )
                } else {
                    Block::gradient3(
                        Block::GRAD3_O_STONE,
                        Block::GRAD3_A_GRASS,
                        Block::GRAD3_B_SNOW,
                        {
                            let vari = 0.3;
                            ((1.0 - overworld.temp).sub(0.65).mul(16.0) - (1.0 - overworld.dry).powf(8.0).mul(4.0))
                                .max(vari)
                                .min(1.0 - vari)
                                .add(overworld.temp_vari * vari)
                                .max(0.0)
                                .min(1.0)
                                .mul(32.0) as u8
                        },
                        {
                            let vari = 0.1;
                            ((650.0 - (z_alt - overworld.z_sea)).div(400.0) - z_warp * 0.1)
                                .max(vari)
                                .min(1.0 - vari)
                                .add(overworld.alt_vari * vari)
                                .max(0.0)
                                .min(1.0)
                                .mul(64.0) as u8
                        },
                    )
                })
            } else {
                Block::STONE
            }
        } else {
            town.block.unwrap_or(if pos_f64.z < overworld.z_water {
                Block::WATER
            } else {
                Block::AIR
            })
        }
    }
}
