// Library
use vek::*;

// Project
use common::terrain::Volume;

trait Vertex = Copy + Clone;

pub struct Mesh<V: Vertex> {
    verts: Vec<V>,
}

impl<V: Vertex> From<Vec<V>> for Mesh<V> {
    fn from(verts: Vec<V>) -> Self { Self { verts } }
}

impl<V: Vertex> Mesh<V> {
    pub fn empty() -> Self { Self { verts: vec![] } }

    pub fn push_vert(&mut self, v: V) { self.verts.push(v); }

    pub fn push_tri(&mut self, tri: [V; 3]) {
        self.verts.push(tri[0]);
        self.verts.push(tri[1]);
        self.verts.push(tri[2]);
    }

    pub fn push_quad(&mut self, quad: [V; 4]) {
        // Tri 0
        self.verts.push(quad[0]);
        self.verts.push(quad[1]);
        self.verts.push(quad[2]);

        // Tri 1
        self.verts.push(quad[2]);
        self.verts.push(quad[3]);
        self.verts.push(quad[0]);
    }
}
