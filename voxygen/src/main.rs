#![feature(
    nll,
    euclidean_division,
    arbitrary_self_types,
    duration_float,
    bind_by_move_pattern_guards,
    trait_alias
)]

// Graphics
#[macro_use]
extern crate gfx;

#[macro_use]
extern crate log;

// Modules
mod camera;
mod game;
mod key_state;
mod keybinds;
mod main_menu;
mod session;
mod tests;
mod ui;
mod window;

// > Rendering
mod consts;
mod hud;
mod pipeline;
mod renderer;
mod shader;

mod mesh;
mod pipelines;
mod volume;

// > Pipelines
mod audio;
mod skybox;
mod tonemapper;
mod voxel;

// Standard
use std::{
    io::{self, Write},
    panic,
    path::{Path, PathBuf},
    thread,
    time::Duration,
};

// Library
use chrono::{DateTime, TimeZone, Utc};
use parking_lot::Mutex;

// Project
use client::PlayMode;
use common::get_version;

// Local
use crate::{game::Game, renderer::RendererInfo};

// START Environment variables
const GIT_HASH: Option<&'static str> = option_env!("GIT_HASH");
const GIT_TIME: Option<&'static str> = option_env!("GIT_TIME");
const PROFILE: Option<&'static str> = option_env!("PROFILE");
const BUILD_TIME: Option<&'static str> = option_env!("BUILD_TIME");

pub fn get_git_hash() -> &'static str { GIT_HASH.unwrap_or("UNKNOWN GIT HASH") }
pub fn get_git_time() -> DateTime<Utc> { Utc.timestamp(GIT_TIME.unwrap_or("-1").to_string().parse().unwrap(), 0) }
pub fn get_profile() -> &'static str { PROFILE.unwrap_or("UNKNOWN PROFILE") }

pub fn get_build_time() -> DateTime<Utc> { Utc.timestamp(BUILD_TIME.unwrap_or("-1").to_string().parse().unwrap(), 0) }
pub fn get_shader_dir() -> &'static Path { Path::new(option_env!("VOXYGEN_SHADERS").unwrap_or("shaders/")) }
// END Environment variables

pub fn get_shader_path(rpath: &str) -> PathBuf { get_shader_dir().join(rpath) }

static RENDERER_INFO: Mutex<Option<RendererInfo>> = Mutex::new(None);

fn set_panic_handler() {
    let default_hook = panic::take_hook();
    panic::set_hook(Box::new(move |details| {
        default_hook(details);
        if let Some(info) = &*RENDERER_INFO.lock() {
            println!(
                "Graphics card info - vendor: {} model: {} OpenGL: {}",
                info.vendor, info.model, info.gl_version
            );
        }
    }));
}

fn main() {
    pretty_env_logger::init();
    set_panic_handler();

    info!("Starting Voxygen... Version: {}", get_version());

    Game::new().run();
}
