use crate::terrain::Voxel;

#[repr(u16)]
#[derive(Copy, Clone, PartialEq)]
pub enum CellMaterial {
    GlossySmooth(u8),
    GlossyRough(u8),
    MatteSmooth(u8),
    MatteRough(u8),
    MetallicSmooth(u8),
    MetallicRough(u8),
    Empty,
}

#[derive(Copy, Clone)]
pub struct Cell {
    mat: CellMaterial,
    pub bone: u8,
}

impl Voxel for Cell {
    type Material = CellMaterial;

    // TODO: WTH is this method doing in the Voxel trait?!
    fn new(mat: Self::Material) -> Self { Cell { mat, bone: 0 } }

    fn empty() -> Self {
        Cell {
            mat: CellMaterial::Empty,
            bone: 0,
        }
    }

    fn is_solid(&self) -> bool { self.mat != CellMaterial::Empty }

    fn material(&self) -> Self::Material { self.mat }
}
