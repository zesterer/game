// Library
use vek::*;

// Project
use common::terrain::chunk::Block;

// Local
use crate::{
    mesh::Mesh,
    volume::{MeshableVoxel, VoxelFace, VoxelLocality},
};

const SUB_BLOCKS_PER_BLOCK: u16 = 11;

gfx_defines! {
    vertex TerrainVertex {
        // Position in sub-blocks (each block is 11 sub-blocks wide)
        pos: [u16; 3] = "vpos",
        // RGBA colour of the block
        col: [u8; 4] = "vcol",
        // Attributes:
        // attr[0] = Normal (XXYYZZ00, -1 <= n <= 2)
        // attr[1] = Material (0 = normal, 1 = water)
        // attr[2] = AO (0-255, % exposure)
        // attr[3] = Emission (LLLLLCCC, 0-31 light level, 8 possible colours)
        attr: [u8; 4] = "vattr",
    }
}

impl MeshableVoxel for Block {
    type Vertex = TerrainVertex;

    fn mesh_face(&self, face: VoxelFace, mesh: &mut Mesh<Self::Vertex>) {
        unimplemented!();

        /*
        self.push_quad([
            Vertex { pos: quad.pos[0].map(|e| e as u16 * SUB_BLOCKS_PER_BLOCK).into_array(), col: quad.col[0].into_array(), attr: [0, 0, 0, 0] },
            Vertex { pos: quad.pos[1].map(|e| e as u16 * SUB_BLOCKS_PER_BLOCK).into_array(), col: quad.col[1].into_array(), attr: [0, 0, 0, 0] },
            Vertex { pos: quad.pos[2].map(|e| e as u16 * SUB_BLOCKS_PER_BLOCK).into_array(), col: quad.col[2].into_array(), attr: [0, 0, 0, 0] },
            Vertex { pos: quad.pos[3].map(|e| e as u16 * SUB_BLOCKS_PER_BLOCK).into_array(), col: quad.col[3].into_array(), attr: [0, 0, 0, 0] },
        ]);
        */
    }
}
