// Standard
use std::{
    cell::Cell,
    sync::atomic::{AtomicBool, Ordering},
};

// Library
use gfx_window_glutin;
use glutin::{
    self,
    dpi::{LogicalPosition, LogicalSize},
    Api::OpenGl,
    ContextBuilder, DeviceEvent, Event as GlutinEvent, EventsLoop, GlContext, GlRequest, GlWindow, WindowBuilder,
    WindowEvent,
};
use parking_lot::{RwLock, RwLockReadGuard, RwLockWriteGuard};
use vek::*;

// Local
use crate::renderer::{ColorFormat, DepthFormat, Renderer, RendererInfo};

// Reexports
pub use glutin::{ElementState, ModifiersState as Modifiers, MouseButton, VirtualKeyCode as KeyCode};

pub enum Event {
    CloseRequest,
    Resize(Vec2<u32>),

    CursorMove(Vec2<f32>),
    CursorAt(Vec2<f32>),
    MouseScroll {
        delta: Vec2<f32>,
        mods: Modifiers,
    },
    MouseButton {
        button: MouseButton,
        state: ElementState,
        pos: Vec2<f32>,
    },

    Keyboard {
        scancode: u32,
        keycode: Option<KeyCode>,
        state: ElementState,
        mods: Modifiers,
    },
    Character(char),
    Raw(GlutinEvent),
}

pub struct RenderWindow {
    events_loop: RwLock<EventsLoop>,
    gl_window: RwLock<GlWindow>,
    renderer: RwLock<Renderer>,
    cursor_trapped: AtomicBool,
    cursor_pos: Cell<Vec2<f32>>,
}

impl RenderWindow {
    pub fn new() -> RenderWindow {
        let events_loop = RwLock::new(EventsLoop::new());
        let win_builder = WindowBuilder::new()
            .with_title("Veloren (Voxygen)")
            .with_dimensions(LogicalSize::new(800.0, 500.0))
            .with_maximized(false);

        let ctx_builder = ContextBuilder::new()
            .with_gl(GlRequest::Specific(OpenGl, (3, 2)))
            .with_multisampling(4)
            .with_vsync(true);

        let (gl_window, device, factory, color_view, depth_view) =
            gfx_window_glutin::init::<ColorFormat, DepthFormat>(win_builder, ctx_builder, &events_loop.read());

        // Workaround for rendering issue on OSX.
        // https://github.com/tomaka/glutin/issues/1069
        events_loop.write().poll_events(|_| {});
        gl_window.resize(glutin::dpi::PhysicalSize::new(800.0, 500.0));

        let size: (u32, u32) = gl_window
            .get_inner_size()
            .unwrap()
            .to_physical(gl_window.get_hidpi_factor())
            .into();

        let rw = RenderWindow {
            events_loop,
            gl_window: RwLock::new(gl_window),
            renderer: RwLock::new(Renderer::new(
                device,
                factory,
                color_view,
                depth_view,
                (size.0 as _, size.1 as _),
            )),
            cursor_trapped: AtomicBool::new(false),
            cursor_pos: Cell::new(Vec2::zero()),
        };
        rw
    }

    pub fn get_renderer_info(&self) -> RendererInfo {
        let renderer = self.renderer.read();
        renderer.get_info()
    }

    pub fn handle_events<F: FnMut(Event) -> bool>(&self, mut f: F) {
        // We need to mutate these inside the closure, so we take a mutable reference
        let gl_window = &mut self.gl_window.read();
        let events_loop = &mut self.events_loop.write();

        events_loop.poll_events(|event| {
            let raw_event = event.clone();

            match event {
                glutin::Event::DeviceEvent { event, .. } => match event {
                    DeviceEvent::MouseMotion { delta: (dx, dy), .. } => {
                        if self.cursor_trapped.load(Ordering::Relaxed) {
                            f(Event::CursorMove(Vec2::new(dx as f32, dy as f32)));
                        }
                    },
                    _ => {},
                },
                glutin::Event::WindowEvent { event, .. } => match event {
                    WindowEvent::Resized(LogicalSize { width, height }) => {
                        let mut color_view = self.renderer.read().color_view().clone();
                        let mut depth_view = self.renderer.read().depth_view().clone();
                        gfx_window_glutin::update_views(&gl_window, &mut color_view, &mut depth_view);
                        let size: (u32, u32) = gl_window
                            .get_inner_size()
                            .unwrap()
                            .to_physical(gl_window.get_hidpi_factor())
                            .into();
                        self.renderer
                            .write()
                            .set_views(color_view, depth_view, (size.0 as _, size.1 as _));
                        f(Event::Resize(Vec2::new(width as u32, height as u32)));
                    },
                    WindowEvent::MouseWheel {
                        delta, modifiers: mods, ..
                    } => {
                        let delta = match delta {
                            glutin::MouseScrollDelta::LineDelta(x, y) => {
                                Vec2::new(f32::from(x) * 8.0, f32::from(y) * 8.0)
                            },
                            glutin::MouseScrollDelta::PixelDelta(LogicalPosition { x, y }) => {
                                Vec2::new(x as f32, y as f32)
                            },
                        };

                        f(Event::MouseScroll { delta, mods });
                    },
                    WindowEvent::KeyboardInput { device_id, input } => {
                        f(Event::Keyboard {
                            scancode: input.scancode,
                            keycode: input.virtual_keycode,
                            state: input.state,
                            mods: input.modifiers,
                        });
                    },
                    WindowEvent::MouseInput { state, button, .. } => {
                        f(Event::MouseButton {
                            button,
                            state,
                            pos: self.cursor_pos.get(),
                        });
                    },
                    WindowEvent::CloseRequested => {
                        f(Event::CloseRequest);
                    },
                    WindowEvent::Focused(is_focused) if !is_focused => self.untrap_cursor(),
                    WindowEvent::CursorMoved { position: pos, .. } => {
                        let pos = Vec2::new(pos.x as f32, pos.y as f32);
                        self.cursor_pos.set(pos);
                        f(Event::CursorAt(pos));
                    },
                    WindowEvent::ReceivedCharacter(c) => {
                        f(Event::Character(c));
                    },
                    _ => {},
                },
                _ => {},
            }

            f(Event::Raw(raw_event));
        });
    }

    pub fn trap_cursor(&self) {
        self.gl_window.read().hide_cursor(true);
        self.gl_window.read().grab_cursor(true).expect("Could not grab cursor!");
        self.cursor_trapped.store(true, Ordering::Relaxed);
    }

    pub fn untrap_cursor(&self) {
        self.gl_window.read().hide_cursor(false);
        self.gl_window
            .read()
            .grab_cursor(false)
            .expect("Could not ungrab cursor!");
        self.cursor_trapped.store(false, Ordering::Relaxed);
    }

    pub fn swap_buffers(&self) {
        self.gl_window
            .read()
            .swap_buffers()
            .expect("Failed to swap window buffers");
    }

    pub fn get_size(&self) -> Vec2<f32> {
        let window = self.gl_window.read();
        match window.get_inner_size() {
            Some(LogicalSize { width: w, height: h }) => Vec2::new(w as f32, h as f32),
            None => Vec2::zero(),
        }
    }

    #[allow(dead_code)]
    pub fn renderer(&self) -> RwLockReadGuard<Renderer> { self.renderer.read() }
    #[allow(dead_code)]
    pub fn renderer_mut(&self) -> RwLockWriteGuard<Renderer> { self.renderer.write() }

    #[allow(dead_code)]
    pub fn cursor_trapped(&self) -> &AtomicBool { &self.cursor_trapped }
}
