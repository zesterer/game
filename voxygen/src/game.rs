// Standard
use std::{
    f32::consts::PI,
    net::ToSocketAddrs,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

// Library
use dot_vox;
use fnv::FnvBuildHasher;
use fps_counter::FPSCounter;
use indexmap::IndexMap;
use parking_lot::Mutex;
use vek::*;

type FnvIndexMap<K, V> = IndexMap<K, V, FnvBuildHasher>;

// Project
use client::{self, Client, ClientEvent, PlayMode, CHUNK_SIZE};
use common::{
    get_asset_path,
    terrain::{
        self,
        chunk::{Chunk, ChunkContainer},
        Container, VolOffs,
    },
    util::manager::Manager,
};

// Local
use crate::{
    camera::Camera,
    consts::{ConstHandle, GlobalConsts},
    get_shader_path,
    hud::{Hud, HudEvent},
    key_state::KeyState,
    keybinds::{Keybinds, VKeyCode},
    main_menu::{MainMenu, MainMenuEvent},
    pipeline::Pipeline,
    session::{InputState, Session},
    shader::Shader,
    skybox, tonemapper, voxel,
    window::{ElementState, Event, KeyCode, RenderWindow},
    RENDERER_INFO,
};

enum GameState {
    MainMenu(MainMenu),
    Session(Session),
}

pub struct Game {
    running: AtomicBool,

    window: RenderWindow,

    input_state: InputState,
    keys: Keybinds,

    fps: FPSCounter,
    last_fps: usize,

    state: GameState,
}

impl Game {
    pub fn new() -> Game {
        let window = RenderWindow::new();
        let info = window.get_renderer_info();

        println!(
            "Graphics card info - vendor: {} model: {} OpenGL: {}",
            info.vendor, info.model, info.gl_version
        );
        *RENDERER_INFO.lock() = Some(info);

        // Init game state
        let state = GameState::MainMenu(MainMenu::new(&mut window.renderer_mut()));

        Game {
            running: AtomicBool::new(true),

            window,

            input_state: InputState::new(),
            keys: Keybinds::new(),

            fps: FPSCounter::new(),
            last_fps: 60,

            state,
        }
    }

    pub fn handle_window_events(&mut self) {
        self.input_state.reset();

        let mut events = vec![];
        self.window.handle_events(|e| {
            events.push(e);
            true
        });

        for event in events {
            let scr_res = self.window.get_size();

            // Important, non-interruptible events are processed first
            match event {
                Event::CloseRequest => self.running.store(false, Ordering::Relaxed),
                Event::Resize(size) => {
                    self.input_state.cam_aspect_ratio = size.x.max(1) as f32 / size.y.max(1) as f32;
                },
                _ => {},
            }

            // Everything else gets sent to the game states...
            if match &mut self.state {
                GameState::Session(session) => session.handle_event(&event, scr_res),
                GameState::MainMenu(main_menu) => main_menu.handle_event(&event, scr_res),
                _ => false,
            } {
                continue;
            }

            // If the event still wasn't handled, we handle it here
            match event {
                Event::CursorMove(delta) if self.window.cursor_trapped().load(Ordering::Relaxed) => {
                    self.input_state.cam_ori_delta += delta * 0.002;
                },
                Event::MouseScroll { delta, .. } => {
                    self.input_state.cam_zoom_delta -= delta.y / 4.0;
                },
                Event::MouseButton { button, state, pos } => {
                    self.window.trap_cursor();
                },
                Event::Keyboard {
                    scancode,
                    keycode,
                    state,
                    mods,
                } => {
                    // Helper variables to clean up code. Add any new input modes here.
                    let general = &self.keys.general;

                    // General inputs -------------------------------------------------------------
                    if keycode == general.pause.map(|k| k.code()) {
                        // Default: Escape (free cursor)
                        self.window.untrap_cursor();
                    } else if keycode == general.use_item.map(|k| k.code()) {
                        // Default: Ctrl+Q (quit) (temporary)
                        // TODO: Have an actual key for this
                        if mods.ctrl {
                            self.running.store(false, Ordering::Relaxed);
                        }
                    } else if keycode == general.toggle_hud.map(|k| k.code()) && state == ElementState::Released {
                        self.input_state.toggle_hud = true;
                    }

                    let pressed = state == ElementState::Pressed;

                    if keycode == general.forward.map(|k| k.code()) {
                        self.input_state.key_up = pressed;
                    } else if keycode == general.left.map(|k| k.code()) {
                        self.input_state.key_left = pressed;
                    } else if keycode == general.back.map(|k| k.code()) {
                        self.input_state.key_down = pressed;
                    } else if keycode == general.right.map(|k| k.code()) {
                        self.input_state.key_right = pressed;
                    } else if keycode == general.jump.map(|k| k.code()) {
                        self.input_state.key_jump = pressed;
                    } else if keycode == general.crouch.map(|k| k.code()) {
                        //self.key_state.fall = pressed;
                    }
                },
                _ => {},
            }
        }

        // Process game state events
        match &mut self.state {
            GameState::MainMenu(main_menu) => {
                for event in main_menu.get_events() {
                    match event {
                        MainMenuEvent::Play { remote, alias, vd } => {
                            self.state = GameState::Session(
                                Session::new(PlayMode::Character, alias, remote, vd, &mut self.window.renderer_mut())
                                    .expect("TODO: Remove this .expect()"),
                            );
                        },
                        _ => {},
                    }
                }
            },
            _ => {},
        }
    }

    pub fn run(&mut self) {
        while self.running.load(Ordering::Relaxed) {
            // --- Handle events ---

            self.handle_window_events();

            match &mut self.state {
                GameState::MainMenu(main_menu) => main_menu.handle_input(&self.input_state),
                GameState::Session(session) => session.handle_input(&self.input_state),
            }

            // --- Update state ---

            match &mut self.state {
                GameState::MainMenu(main_menu) => main_menu.maintain(),
                GameState::Session(session) => session.maintain(&mut self.window.renderer_mut(), self.last_fps),
            }

            // --- Render ---
            self.window.renderer_mut().begin_frame(None);

            match &self.state {
                GameState::MainMenu(main_menu) => main_menu.render(&mut self.window.renderer_mut()),
                GameState::Session(session) => session.render(&mut self.window.renderer_mut()),
            }

            self.window.swap_buffers();
            self.window.renderer_mut().end_frame();

            // --- Update FPS counter ---

            self.last_fps = self.fps.tick();
        }
    }
}
