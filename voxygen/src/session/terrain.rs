// Standard
use std::{collections::HashMap, sync::mpsc};

// Library
use threadpool::ThreadPool;
use vek::*;
// TODO: Get rid of these
use fnv::FnvBuildHasher;
use indexmap::IndexMap;

// Project
use client::{Client, Payloads, CHUNK_SIZE};
use common::terrain::{voloffs_to_voxabs, ChunkMgr};

// Local
use crate::{consts::ConstHandle, renderer::Renderer, voxel};

// TODO: Just use a mesh for this
type FnvIndexMap<K, V> = IndexMap<K, V, FnvBuildHasher>;
type Mesh = FnvIndexMap<voxel::MaterialKind, voxel::Mesh>;

pub enum Entry {
    BeingMeshed,
    Model {
        // TODO: Have a unified way of storing this data together for model/const patterns
        // ModelStore<Model, GlobalConsts, LocalConsts>?
        model: voxel::Model,
        consts: ConstHandle<voxel::ModelConsts>,
    },
}

pub struct TerrainMgr {
    entries: HashMap<Vec3<i32>, Entry>,
    pool: ThreadPool,
    pool_mpsc: (mpsc::Sender<(Vec3<i32>, Mesh)>, mpsc::Receiver<(Vec3<i32>, Mesh)>),
}

impl TerrainMgr {
    pub fn new() -> Self {
        Self {
            entries: HashMap::new(),
            pool: ThreadPool::new(1),
            pool_mpsc: mpsc::channel(),
        }
    }

    pub fn maintain<P: Payloads>(&mut self, renderer: &mut Renderer, client: &Client<P>) {
        return;

        while let Ok((pos, mesh)) = self.pool_mpsc.1.try_recv() {
            // Utility function
            // TODO: Find a way to get rid of this ugliness
            fn to_4x4(m: &Mat4<f32>) -> [[f32; 4]; 4] {
                let mut out = [[0.0; 4]; 4];
                (0..4).for_each(|i| (0..4).for_each(|j| out[i][j] = m[(j, i)]));
                out
            }

            let consts = ConstHandle::new(renderer);
            consts.update(
                renderer,
                voxel::ModelConsts {
                    model_mat: to_4x4(&Mat4::translation_3d(
                        voloffs_to_voxabs(pos, CHUNK_SIZE).map(|e| e as f32),
                    )),
                },
            );

            self.entries.insert(
                pos,
                Entry::Model {
                    model: voxel::Model::new(renderer, &mesh),
                    consts,
                },
            );
        }

        if let Some(player_entity) = client.player_entity() {
            let player_chunk = player_entity
                .read()
                .pos()
                .map2(CHUNK_SIZE, |e, s| (e as i64).div_euc(s as i64) as i32);

            // Produce a sorted list of the closest chunks
            let vd = (client.view_distance() as i64 / CHUNK_SIZE.x as i64) as i32 + 1;
            let mut chunk_pos_vec: Vec<_> = (-vd..=vd)
                .map(move |x| (-vd..=vd).map(move |y| (-vd..=vd).map(move |z| player_chunk + Vec3::new(x, y, z))))
                .flatten()
                .flatten()
                .collect();
            chunk_pos_vec
                .sort_unstable_by(|a, b| a.distance_squared(player_chunk).cmp(&b.distance_squared(player_chunk)));

            for chunk_pos in chunk_pos_vec.into_iter() {
                if self.entries.contains_key(&chunk_pos) {
                    continue;
                }

                let chunk_from = chunk_pos.map(|e| e as i64) * CHUNK_SIZE.map(|e| e as i64) - Vec3::one();
                let chunk_to = (chunk_pos + 1).map(|e| e as i64) * CHUNK_SIZE.map(|e| e as i64) + Vec3::one();
                if client.chunk_mgr().try_get_sample(chunk_from, chunk_to).is_ok() {
                    self.entries.insert(chunk_pos, Entry::BeingMeshed);

                    let send_ref = self.pool_mpsc.0.clone();
                    let chunk_mgr_ref = client.chunk_mgr().clone();
                    self.pool.execute(move || {
                        if let Ok(sample) = chunk_mgr_ref.try_get_sample(chunk_from, chunk_to) {
                            send_ref.send((chunk_pos, voxel::Mesh::from(&sample)));
                        }
                    });
                }
            }

            self.entries.retain(|&pos, _| {
                (pos.map(|e| e as i64) * CHUNK_SIZE.map(|e| e as i64))
                    .distance_squared(player_entity.read().pos().map(|e| e as i64))
                    > (client.view_distance() as i64 + CHUNK_SIZE.x as i64).pow(2)
            });
        }

        println!(
            "CHUNKS: (meshed = {}, total = {})",
            self.entries
                .values()
                .filter(|&e| std::mem::discriminant(e) == std::mem::discriminant(&Entry::BeingMeshed))
                .count(),
            self.entries.values().count(),
        );
    }

    pub fn render(&self, renderer: &mut Renderer) {}
}
