// Library
use vek::*;

// Project
use common::terrain::{ReadVolume, Volume, Voxel};

// Local
use crate::mesh::Mesh;

#[derive(Copy, Clone)]
pub enum VoxelFace {
    Left,   // -x
    Right,  // +x
    Back,   // -y
    Front,  // +y
    Bottom, // -z
    Top,    // +z
}

pub struct VoxelLocality {
    pub occlusion: [[[u8; 3]; 3]; 3],
}

pub trait MeshableVoxel: Voxel {
    type Vertex: Copy + Clone;

    fn mesh_face(&self, face: VoxelFace, mesh: &mut Mesh<Self::Vertex>);
}

pub trait MeshableVolume: ReadVolume
where
    Self::VoxelType: MeshableVoxel,
{
    fn mesh(
        &self,
        pfrom: Vec3<i64>,
        pto: Vec3<i64>,
    ) -> Vec<(u32, Mesh<<<Self as Volume>::VoxelType as MeshableVoxel>::Vertex>)> {
        // Early exit if the chunk is empty
        // TODO: Should be is_transparent(), not !is_solid() - what about fluids?!
        if self.is_homo().map(|b| !b.is_solid()).unwrap_or(false) {
            return vec![];
        }

        let mut meshes = vec![];
        for layer in 0..2 {
            let mut cmesh = Mesh::empty();

            // TODO: Compute mesh here

            meshes.push((layer, cmesh));
        }

        meshes
    }
}
