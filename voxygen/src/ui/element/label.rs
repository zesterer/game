// Standard
use std::{
    cell::{Cell, Ref, RefCell},
    rc::Rc,
};

// Library
use vek::*;

// Local
use super::{primitive::draw_text, Bounds, Element, ResCache, Span};
use crate::renderer::Renderer;

#[allow(dead_code)]
#[derive(Clone)]
pub struct Label {
    text: RefCell<Option<String>>,
    col: Cell<Rgba<f32>>,
    bg_col: Cell<Rgba<f32>>,
    margin: Cell<Vec2<Span>>,
    size: Cell<Vec2<Span>>,
}

impl Label {
    #[allow(dead_code)]
    pub fn new() -> Rc<Self> {
        Rc::new(Self {
            text: RefCell::new(None),
            col: Cell::new(Rgba::new(0.0, 0.0, 0.0, 1.0)),
            bg_col: Cell::new(Rgba::new(1.0, 1.0, 1.0, 1.0)),
            margin: Cell::new(Span::zero()),
            size: Cell::new(Span::px(16, 16)),
        })
    }

    #[allow(dead_code)]
    pub fn with_text(self: Rc<Self>, text: String) -> Rc<Self> {
        *self.text.borrow_mut() = Some(text);
        self
    }

    #[allow(dead_code)]
    pub fn with_color(self: Rc<Self>, col: Rgba<f32>) -> Rc<Self> {
        self.col.set(col);
        self
    }

    #[allow(dead_code)]
    pub fn with_size(self: Rc<Self>, size: Vec2<Span>) -> Rc<Self> {
        self.size.set(size);
        self
    }

    #[allow(dead_code)]
    pub fn with_margin(self: Rc<Self>, margin: Vec2<Span>) -> Rc<Self> {
        self.margin.set(margin);
        self
    }

    #[allow(dead_code)]
    pub fn get_text(&self) -> Ref<Option<String>> { self.text.borrow() }
    #[allow(dead_code)]
    pub fn set_text(&self, text: String) { *self.text.borrow_mut() = Some(text); }

    #[allow(dead_code)]
    pub fn get_color(&self) -> Rgba<f32> { self.col.get() }
    #[allow(dead_code)]
    pub fn set_color(&self, col: Rgba<f32>) { self.col.set(col); }

    #[allow(dead_code)]
    pub fn get_margin(&self) -> Vec2<Span> { self.margin.get() }
    #[allow(dead_code)]
    pub fn set_margin(&self, margin: Vec2<Span>) { self.margin.set(margin); }

    #[allow(dead_code)]
    pub fn get_size(&self) -> Vec2<Span> { self.size.get() }
    #[allow(dead_code)]
    pub fn set_size(&self, size: Vec2<Span>) { self.size.set(size); }

    #[allow(dead_code)]
    pub fn clone_all(&self) -> Rc<Self> { Rc::new(self.clone()) }

    fn bounds_for_child(&self, scr_res: Vec2<f32>, bounds: Bounds) -> Bounds {
        let margin_rel =
            self.margin.get().map(|e| e.rel) * bounds.1 * scr_res + self.margin.get().map(|e| e.px as f32) / scr_res;
        (bounds.0 + margin_rel, bounds.1 - margin_rel * 2.0)
    }
}

impl Element for Label {
    fn deep_clone(&self) -> Rc<dyn Element> { self.clone_all() }

    fn render(&self, renderer: &mut Renderer, rescache: &mut ResCache, bounds: Bounds) {
        if let Some(text) = self.text.borrow().as_ref() {
            let res = renderer.get_view_resolution().map(|e| e as f32);
            let sz = self.size.get().map(|e| e.rel) * res.map(|e| e as f32) + self.size.get().map(|e| e.px as f32);
            let bounds = self.bounds_for_child(res, bounds);
            draw_text(renderer, rescache, text, bounds.0, sz, self.col.get());
        }
    }
}
